# Nedo_3.x_Parser

This folder contains all the parsers written. This includes:
1. Parser for JSON to CSV - CADVISOR, NECPM, KSMPM - MinIO Bucket
2. Parser for JSON to CSV - CADVISOR, NECPM, KSMPM - Databus Output

3. Parser for JSON to CSV - Baremetal - MinIO Bucket
4. Parser for JSON to CSV - Baremetal - Databus Output

5. Parser for Altiostar XML file format - 32.436 V15.0 to CSV
6. Parser for LTE XML file format - 32.435 V7.0 to CSV

7. Get CSV for any counter with all timestamps that are populated in the CSV created by parsers above. (Baremetal,CADVISOR, NECPM, KSMPM, ALTIOSTAR)

## Getting started

Every Parser will mention the set of inputs that the user needs to give. Please give those details before running the parser. This information will be available at the top of the code file. 

## Files corresponding to the parser
1. Parser for JSON to CSV - CADVISOR, NECPM, KSMPM - MinIO Bucket - https://gitlab.com/kruthika.kulkarni/nedo_3.x_parser/-/blob/main/Code/Parser-Json-Common.py

This code can be used for CADVISOR, NECPM, KSMPM Json files from MinIO bucket. The user can specify the list of features that he wants or let all features be pulled from JSON to CSV. 

2. Parser for JSON to CSV - CADVISOR, NECPM, KSMPM - Databus Output - https://gitlab.com/kruthika.kulkarni/nedo_3.x_parser/-/blob/main/Code/Parsere-JSON-DatabusData-20220810.py

This code is different from the above as the databus dumps 1000 lines (json format) as strings to the VM. This code can be used for CADVISOR, NECPM, KSMPM.

3. Parser for JSON to CSV - Baremetal - MinIO Bucket - https://gitlab.com/kruthika.kulkarni/nedo_3.x_parser/-/blob/main/Code/Parser-Json-Baremetal.py

This code can be used for Baremetal Json files from MinIO bucket. The user can specify the list of features that he wants or let all features be pulled from JSON to CSV. Baremetal data parser is different from other three as they have values as array.

4. Parser for JSON to CSV - Baremetal - Databus Output - https://gitlab.com/kruthika.kulkarni/nedo_3.x_parser/-/blob/main/Code/Parser-Baremetal-DatabusData-20220810.py

This code is different from the above as the databus dumps 1000 lines (json format) as strings to the VM. This code can be used for Baremetal

5. Parser for Altiostar XML file format - 32.436 V15.0 to CSV - https://gitlab.com/kruthika.kulkarni/nedo_3.x_parser/-/blob/main/Code/Parser-Altiostar-32.436-V15.0.py


6. Parser for LTE XML file format - 32.435 V7.0 to CSV - https://gitlab.com/kruthika.kulkarni/nedo_3.x_parser/-/blob/main/Code/Parser-LTE-32.435-V7.0.py


7. Get CSV for any counter with all timestamps that are populated in the CSV created by parsers above. (Baremetal,CADVISOR, NECPM, KSMPM, Altiostar ) - https://gitlab.com/kruthika.kulkarni/nedo_3.x_parser/-/blob/main/Code/GetCSVForFeatures.py


## Project status
1. The codes need to be tested by other users
2. Any optimization or code changes suggested by the team will be implemented. 
