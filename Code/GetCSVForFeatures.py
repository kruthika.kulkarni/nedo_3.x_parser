#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 18 16:33:00 2022

@author: kruthika.kulkarni
"""

import glob

import warnings
warnings.filterwarnings('ignore')
import pandas as pd
import time
import os
# from datetime import datetime, timedelta
from multiprocess import Pool

def main():
    """
    csv_files : List of files from which you want to create certain counter based CSV
    feat : List of features for which CSV needs to be created. If an empty list is given, CSV for all features in the parsed csv will be created. 
    counter_name_col : Name of the column in the parsed csv which contains the counter name information. eg: performanceMetric
    counter_value_col : Name of the column in the parsed csv which contains the counter value information. eg: value in NECPM
    output_path : Location where the feature based CSV should be stored. 
    lst_of_cols : Specifies which data you are working with to choose the columns in your file accordingly. eg: cadvisor. The value of this will be one of the following lists mentioned below : [baremetal, cadvisor,necpm_amf,necpm_smf,necpm_upf, ksmpm, altiostar]
    """

    start = time.time()
    # set of files to fetch for the feature
    csv_files = glob.glob("/Users/kruthika.kulkarni/Documents/Projects/Nedo-3.x/Nedo3.2/CADVISOR/CSV/*.csv")
    # features to get CSV for 
    # feat = pd.read_csv("/Users/kruthika.kulkarni/Documents/Projects/Nedo-3.x/Parser_other/NECPM_AMF_Useable_Feat.csv")\
    #     ['KPI'].unique()
    feat = []
        
    counter_name_col = 'performanceMetric'
    counter_value_col = 'value'
    output_path = "/Users/kruthika.kulkarni/Documents/Projects/Nedo-3.x/Nedo3.2/CADVISOR/CSV_Features/Check3/"
    
    #Baremetal
    baremetal = ['objectInstanceId',	'objectType',	'performanceMetric',	\
                   'dsnames',	'plugin_instance',	'type_instance',	'timeStamp',	\
                   'values',	'subObjectInstanceId']
    #CADVISOR
    cadvisor = ['timeStamp',
 'kubernetes_namespace',
 'processTime',
 'id',
 'robin_io_robinrpool',
 'robin_io_domain',
 'node',
 'obf_ns_id',
 'counter_category',
 'kubernetes_io_hostname',
 'robin_io_nodetype',
 'beta_kubernetes_io_os',
 'kubernetes_io_os',
 'objectType',
 'namespace',
 'robin_io_robinhost',
 'subObjectInstanceId',
 'objectInstanceId',
 'kubernetes_io_arch',
 'enriched',
 'instance',
 'app_kubernetes_io_instance',
 'cluster_type',
 'obf_cnfc_id',
 'name',
 'app_kubernetes_io_version',
 'cpu',
 'pod',
 'container',
 'cluster_name',
 'robin_io_hostname',
 'app_kubernetes_io_name',
 'value',
 'job',
 'kubernetes_name',
 'image',
 'beta_kubernetes_io_arch',
 'app_kubernetes_io_managed_by',
 'sub_category',
 'obf_cnf_id',
 'helm_sh_chart',
 '__name__',
 'robin_io_rnodetype',
 'performanceMetric']
    # cadvisor = ['objectInstanceId',
    # 'performanceMetric',
    # 'timeStamp',
    # 'container',
    # 'cluster_name',
    # 'instance',
    # 'pod',
    # 'sub_category',
    # 'cluster_type',
    # 'processTime',
    # # 'id',
    # # 'namespace',
    # 'value',
    # 'subObjectInstanceId',
    # 'objectType']
    #NECPM AMF
    necpm_amf = ['objectInstanceId', 'performanceMetric', 'timeStamp', 'app', 'cluster_name', \
                   'obf_cnfc_id', 'instance', 'sub_category', 'cnfc_uuid', 'ns_uuid', 'counter_category', \
                   'cluster_type', 'obf_cnf_id', 'nsi_id', 'kubernetes_pod_name', 'nssi_id', \
                   'kubernetes_namespace', 'sd', 'obf_ns_id', '__name__', 'sst', 'job', 'cnf_uuid', \
                   'value', 'subObjectInstanceId', 'objectType']
    
    #NECPM SMF
    necpm_smf = ['objectInstanceId', 'performanceMetric', 'timeStamp', 'instance', 'istio_io_rev',\
                   'destination_app', 'destination_workload', 'source_principal', 'cluster_type', \
                   'kubernetes_namespace', 'sst', 'connection_security_policy', 'cnf_uuid', 'request_protocol',\
                   'app', 'destination_version', 'obf_cnfc_id', 'obf_cnf_id', 'service_istio_io_canonical_revision',\
                   'source_canonical_service', 'version', 'kubernetes_pod_name', '__name__', 'name', \
                   'source_app', 'job', 'source_cluster', 'source_workload', 'destination_principal',\
                   'destination_cluster', 'response_code', 'destination_canonical_service', \
                   'service_istio_io_canonical_name', 'sub_category', 'destination_canonical_revision',\
                   'nsi_id', 'source_canonical_revision', 'nssi_id', 'source_version', 'sd', \
                   'destination_service', 'destination_service_name', 'cluster_name', 'cnfc_uuid', \
                   'ns_uuid', 'source_workload_namespace', 'counter_category', 'pod_template_hash', \
                   'reporter', 'destination_service_namespace', 'destination_workload_namespace', \
                   'response_flags', 'obf_ns_id', 'security_istio_io_tlsMode', 'le', 'value', \
                   'subObjectInstanceId', 'objectType']
    #NECPM UPF
    necpm_upf = ['objectInstanceId', 'performanceMetric', 'timeStamp',
           'app', 'cluster_name', 'obf_cnfc_id', 'instance', 'sub_category',
           'release', 'cnfc_uuid', 'ns_uuid', 'counter_category',
           'pod_template_hash', 'cluster_type', 'obf_cnf_id',
           'force_slice_enrichment', 'nsi_id',  'nssi_id',
           'kubernetes_namespace', 'sd', 'obf_ns_id', '__name__', 'sst', 'job',
           'cnf_uuid', 'value', 'subObjectInstanceId', 'objectType']
    
    #KSMPM
    ksmpm = ['objectInstanceId', 'performanceMetric', 'timeStamp', 'app_kubernetes_io_managed_by',\
                   'instance', 'pod', 'sub_category', 'cluster_type', 'processTime', 'kubernetes_namespace', \
                   'app_kubernetes_io_name', 'app_kubernetes_io_instance', 'cluster_name', 'obf_cnfc_id', \
                   'kubernetes_name', 'counter_category', 'obf_cnf_id', 'obf_ns_id', 'helm_sh_chart', \
                   '__name__', 'app_kubernetes_io_version', 'namespace', 'job', 'value', 'subObjectInstanceId',\
                   'objectType']
        
    altiostar = ['counter',
'DU',
'measInfoId',
'measTimeStamp',
'granularityPeriod',
'Value',
'reportingPeriod',
'jobId',
'collectionBeginTime',
'softwareVersion',
'measFileFooter',
'vendorName',
'fileFormatVersion',
'senderType',
'senderName',
'distinguishedName']
        
    p = Pool(4)
    #Specify which dataset you are working with
    lst_of_cols = cadvisor
    arguments = [(lst_of_cols, x, output_path, feat, \
                  counter_name_col,counter_value_col) for x in csv_files]
    p.starmap(kpi_wise_csv, arguments)
    end = time.time()
    print("Total Time: ", end - start)
    
    

def kpi_wise_csv(lst_of_cols, file, output_path, feature_list,\
                 counter_name_col,counter_value_col):
    """
    lst_of_cols : Columns to be retained in the CSV to ensure that no two rows are duplicates. 
                  This is fixed and the values in the next cell are to be used. 
    file : CSv file to be checked to get data for the feat.
    feat : Feature list for which you need CSV. A dataframe for every feature.
    output_path : Path where the CSv for each feature should be stored.
    
    """
    

    df = pd.read_csv(file)
    if (df.shape[0] != 0) and (counter_name_col in df.columns):
        lst = df[counter_name_col].unique()
        if len(feature_list) > 0:
            lst = [x for x in lst if x in feature_list]
        for j,feat in enumerate(lst):
            temp = df[df[counter_name_col] == feat]
            try:
                temp = temp[lst_of_cols]
            except KeyError:
                bad_col = [x for x in lst_of_cols if x not in temp.columns ]
                for col in bad_col:
                    temp[col] = float("NaN")
                temp = temp[lst_of_cols]
                

            temp.rename(columns = {counter_value_col:feat}, inplace = True)
            temp.drop(columns = [counter_name_col], inplace = True)

            file_name = output_path+feat+".csv"
            if os.path.isfile(file_name):

                temp.to_csv(file_name,index=False, mode = 'a', header = False)
            else:
                temp.to_csv(file_name, index=False)
                
if __name__ == '__main__':
    main()

        
    
