#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 18 14:33:04 2022

@author: kruthika.kulkarni
"""

import xml.etree.ElementTree as ETree
import pandas as pd
import glob
import time
from multiprocess import Pool
from datetime import datetime, timedelta

def main():
    start = time.time()
    p = Pool(4)
    # Provide the list of xmls here
    list_of_files = glob.glob("/Users/kruthika.kulkarni/Documents/Projects/Nedo-3.x/Temp/*.xml")
    # Provide the path to store the output
    output_path = "/Users/kruthika.kulkarni/Documents/Projects/Nedo-3.x/Temp/"
    arguments = [(x,output_path) for x in list_of_files]
    p.starmap(get_xml, arguments)
    end = time.time()
    print(end - start)
    
def get_xml(file_path, output_path):
    output_path = output_path +file_path.split("/")[-1][:-4]+".csv"
    df = pd.DataFrame()
    prstree = ETree.parse(file_path)
    root = prstree.getroot()
    data = [x for x in root if x.tag == '{http://www.3gpp.org/ftp/specs/archive/32_series/32.435#measCollec}measData']
    if len(data) > 1:
        print("Issue detected in number of elements in data")
    for ele in data:
        # data_managed_ele = [x for x in ele if x.tag == \
        #                     '{http://www.3gpp.org/ftp/specs/archive/32_series/32.435#measCollec}managedElement'][0]

        # managed_ele_cols = data_managed_ele.attrib.keys()
        # managed_ele_values = data_managed_ele.attrib.values()
        data_measInfo = [x for x in ele if x.tag == \
                        '{http://www.3gpp.org/ftp/specs/archive/32_series/32.435#measCollec}measInfo']
        for info_ele in data_measInfo:
            if df.shape[0] == 0:
                df = xml_info3(info_ele)
            else:
                temp = xml_info3(info_ele)
                df = df.append(temp)
    df.drop(columns = ['duration','duration_1'], inplace = True)
    df.to_csv(output_path, index = False)

def convert_time(s):
    s1 = str(s.values[0])
    time_new = datetime.strptime(s1[-5:], '%H:%M').time()
    return datetime.fromisoformat(s1[:-6])+timedelta(hours = time_new.hour,minutes=time_new.minute)


def xml_info3(ele):
    cols = list(ele.attrib.keys())
    values = list(ele.attrib.values())
    feat_lst = []
    val_lst = []
    network_lst = []
    final = pd.DataFrame()
    lst_of_kpi_val = {}
    initial_counters = []
    initial_network_elements = []
    for child in ele:
        if (type(child.attrib) == dict) and child.tag != \
        '{http://www.3gpp.org/ftp/specs/archive/32_series/32.435#measCollec}measValue':
            
            cols = cols + list(child.attrib.keys())
            values = values + list(child.attrib.values())
        if (type(child.text) == str) and child.tag == \
        '{http://www.3gpp.org/ftp/specs/archive/32_series/32.435#measCollec}measTypes':

            initial_counters =  child.text.split(" ")
        elif (type(child.text) == str) and child.tag == \
        '{http://www.3gpp.org/ftp/specs/archive/32_series/32.435#measCollec}measValue':
            
            for grandchild in child:
                if grandchild.text != 'true':
                    initial_network_elements = list(child.attrib.values())
                    network_lst = network_lst + initial_network_elements * len(grandchild.text.split(" "))
                    feat_lst = feat_lst + initial_counters

                    val_lst = val_lst + (grandchild.text.split(" "))
                    
    lst_of_kpi_val['measObjLdn'] = network_lst
    lst_of_kpi_val['measTypes'] = feat_lst
    lst_of_kpi_val['Values'] = val_lst

    final = pd.DataFrame.from_dict(lst_of_kpi_val,orient='index').transpose()

    for i,val in enumerate(cols):
        if len([x for x in cols if x == val]) > 1:
            cols[i] = val+"_"+str(i)
        
    for i,col in enumerate(cols):
        final[col] = values[i]
        
    final['TimeStamp']=final[['endTime']].apply(convert_time, axis = 1)
    final.drop(columns = ['endTime'], inplace = True)
    return final

if __name__ == '__main__':
    main()