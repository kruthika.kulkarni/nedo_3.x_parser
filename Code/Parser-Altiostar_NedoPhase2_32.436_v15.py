#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug  9 16:17:20 2022

@author: kruthika.kulkarni
"""

import xml.etree.ElementTree as ETree
import pandas as pd
import glob
import time
from multiprocess import Pool
from datetime import datetime, timedelta
import re
import numpy as np



# PM files
def main():
    """
    files : Give the list of XML files to be parsed
    output_path : Location where the parsed CSV should be saved.
    p : Change the number of Pool based on your VM capacity.
    
    """
    start = time.time()
    files = glob.glob("")
    output_path = "/Users/kruthika.kulkarni/Documents/Projects/"
    p = Pool(4)
    
    arguments = [(file,output_path) for file in files]
    p.starmap(get_csv, arguments)
    end = time.time()
    print("Time Taken:", end - start)

def get_csv(file,output_path):
    prstree = ETree.parse(file)
    root = prstree.getroot()
    lst_check = xml_recursive(root, [])
    metakey = []
    metavalue = []
    lst_check = [x for x in lst_check if len(x)>0]
    lst_check = [{list(x.keys())[0].split("}")[-1]:list(x.values())[0]} for x in lst_check]
    for ele in lst_check:
        if 'value' in list(ele.keys())[0].lower():
            ele['Value'] = ele[list(ele.keys())[0]]
            del ele[list(ele.keys())[0]]
            
    lst_of_keys = list(set([list(x.keys())[0] for x in lst_check]))
    for key in lst_of_keys:
        if len([list(x.keys())[0] for x in lst_check if list(x.keys())[0] == key]) == 1:
            metakey.append(key)
            metavalue.append([list(x.values())[0] for x in lst_check if list(x.keys())[0] == key][0])
    df = pd.DataFrame()
    rows = []
    temp = []
    for l,ele in enumerate(lst_check):

        if list(ele.keys())[0] not in metakey:
            if (list(ele.keys())[0] == 'measInfoId') and len(temp) > 0:
                rows.append(temp)
                temp = []
            temp.append(ele)
            
        if l == len(lst_check) - 1:
            rows.append(temp)

    for ele in rows:
        completed_tags = []
        temp_df = pd.DataFrame()

#         num_of_rows =  num_of_counters * num_of_du

        counters = [list(x.values())[0] for x in ele if 'measTypes' in x.keys()][0].split(" ")
        value_of_du = [list(x.values())[0] for x in ele if 'measObjLdn' in x.keys()]
        num_of_counters = len(counters)
        num_of_du = len(value_of_du)
        num_of_rows =  num_of_counters * num_of_du
        counters = counters * num_of_du
        temp_df = pd.DataFrame(np.array(counters))
        completed_tags.append('measTypes')
        temp_df.columns = ["counter"]

        for i in range(num_of_du):
            temp_df.loc[i*num_of_counters : (i+1)*num_of_counters-1, 'DU'] = value_of_du[i]
        completed_tags.append('measObjInstId')
        
        for item in ele:
            key_item = [list(x.keys())[0] for x in ele if list(item.keys())[0] in x.keys()]
            value_item = [list(x.values())[0] for x in ele if list(item.keys())[0] in x.keys()]
            if key_item[0] not in completed_tags:
                if key_item[0] != 'measResults':
                    temp_df[list(item.keys())[0]] = list(item.values())[0]
                    completed_tags.append(key_item[0])
                else:
                    final_list_of_values = []
                    value_item = [x.split(" ") for x in value_item]
                    for val in value_item:
                        final_list_of_values = final_list_of_values + val
                    if len(final_list_of_values) == num_of_rows:
                        temp_df[key_item[0]] = np.array(final_list_of_values)
                        completed_tags.append(key_item[0])
                    else:
                            print("Doesn't work, values not same as types")
        if df.shape[0] == 0:
            df = temp_df
        else:
            df = df.append(temp_df)

    for i,key in enumerate(metakey):
        df[key] = metavalue[i]
        
    df.to_csv(output_path+file.split('/')[-1].replace('xml','csv'), index = False)
    

            
def xml_recursive(root, lst):
    main_dict = {}
    if root.text == None:
        return {}
    elif (len(re.findall(r'[^\n" "]', root.text)) > 0) and (len(root.attrib) == 0) and \
    len(root) == 0:
        main_dict[root.tag] = root.text
        return main_dict
    elif (len(re.findall(r'[^\n" "]', root.text)) > 0) and (len(root.attrib) > 0) and \
    len(root) == 0:
        main_dict[root.tag] = root.text
        for key,val in root.attrib:
            main_dict[key] = val
        return main_dict
    elif (len(re.findall(r'[^\n" "]', root.text)) == 0) and (len(root.attrib) > 0) and \
    len(root) == 0:
        for key,val in root.attrib:
            main_dict[key] = val
        return main_dict
    elif len(root) > 0:
        if len(root.attrib) > 0:
            for key,val in root.attrib.items():
                lst.append({key:val})
        for i,child in enumerate(root):
            temp = xml_recursive(child, lst)
            if (type(temp) == dict) and len(temp) > 0:
                lst.append(temp)
            
            
    return lst    

if __name__ == '__main__':
    main()        
            
            
            
        
