#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 18 16:28:52 2022

@author: kruthika.kulkarni
"""



import glob

import warnings
warnings.filterwarnings('ignore')
import pandas as pd
import time
import json

from datetime import datetime, timedelta
from multiprocess import Pool

def main():
    start = time.time()
    # List all json files to be converted to csv
    json_files = glob.glob("/Users/kruthika.kulkarni/Documents/Projects/Nedo-3.x/CADVISOR/*.json")
    # Output_path to store csv
    output_path = "/Users/kruthika.kulkarni/Documents/Projects/Nedo-3.x/CADVISOR/cadvisor"
    # List of Features
    feat_list = ['container_fs_io_time_weighted_seconds_total',
'container_network_receive_errors_total',
'container_network_transmit_packets_dropped_total',
'container_network_receive_packets_dropped_total',
'container_network_transmit_bytes_total',
'container_network_receive_packets_total',
'container_network_receive_bytes_total',
'container_fs_io_time_seconds_total',
'container_network_transmit_packets_total']
    # indicator if a csv was created
    k = 0
    #indicator for name 
    i = 0
    print(len(json_files))
    p = Pool(4)
    arguments = [(file,feat_list) for file in json_files]
    results = p.starmap(baremetal_data, arguments)
    final = pd.DataFrame()
    for df in results:

        if final.shape[0] == 0:
            final = df
        else:
            final = final.append(df)
        
        if final.shape[0] > 300000:
            k = 1
            final.to_csv(output_path+"_v"+str(i)+".csv", index = False)
            final = pd.DataFrame()
            i += 1
        else:
            k = 0
    if k == 0:
        final.to_csv(output_path+"_v"+str(i)+".csv", index = False)
    end = time.time()
    print("Time Taken: ", end - start)
    
def baremetal_data(file, lst_of_feat):
    lst_for_df = []
    for line in open(file, 'r'):
            if any(ext in line for ext in lst_of_feat):
                try:
                    item = json.loads(line)
                except ValueError:
                    continue
                if item['value']['entries']['array'] is None:
                    pass
                else:
                    for val in item['value']['entries']['array']:
                        lst = []
                        dict_to_consider = {}
                        final = json_recursion(val,"text",lst)
                        final = [x for x in final if type(x) is dict]
                        for ele in final:
                            dict_to_consider[list(ele.keys())[0]] = list(ele.values())[0]
                        lst_for_df.append(dict_to_consider)
                        

                
    final = pd.DataFrame(lst_for_df)
    # final['timeStamp1']=final[['timeStamp']].apply(convert_time, axis = 1)
    # final.drop(columns = ['timeStamp'], inplace = True)
    return final        


def convert_time(s):
    s1 = str(s.values[0])
    time_new = datetime.strptime(s1[-5:], '%H:%M').time()
    return datetime.fromisoformat(s1[:-6])+timedelta(hours = time_new.hour,minutes=time_new.minute)


def json_recursion(dictionery,key_value,lst):
    """
    dictionery : Json Line which is being converted to a row in dataframe (dict)
    key_value : the main key of the above dictionery (string)
    lst : The list keeping track of nested values of the dictionery which make up the row of the dataframe.
          The user has to always give an empty list. It gets filled through recursion. 
          Hence we do not need to pass this argument in any function.
    
    """
#     print(dictionery,"\n",key_value,"Key",lst,"Initial \n\n")
    main_dct = {}
    if (len(dictionery.keys()) == 1) and (type(dictionery[list(dictionery.keys())[0]]) != dict) and \
    (type(dictionery[list(dictionery.keys())[0]]) != list):
#         return key_value, dictionery[list(dictionery.keys())[0]]
        main_dct[key_value] = dictionery[list(dictionery.keys())[0]]
#         print(main_dct,"Done")
        return main_dct
        
    elif len([x for x in list(dictionery.values()) if type(x) == str]) == len(list(dictionery.values())):
#         print("Comes here")
        for key_in_dict, value_in_dict in dictionery.items():
#             print(key_in_dict, value_in_dict, "key_value_pair")
            lst.append(json_recursion({key_in_dict:value_in_dict},key_in_dict ,lst))
    else:
        for key in dictionery.keys():
#             print(key,dictionery[key],"Key inside")
            if (type(dictionery[key]) != dict) and (type(dictionery[key]) != list):
                lst.append(json_recursion({key:dictionery[key]},key ,lst))
            if type(dictionery[key]) == dict:
#                 key_ans, value_ans = json_recursion(dictionery[key], key,lst)
#                 lst[key_ans] = value_ans
                lst.append(json_recursion(dictionery[key], key,lst))
            elif type(dictionery[key]) == list:
#                 print("Inside list", key)
                for element in dictionery[key]:
                    if type(element) == dict:
#                         print(element.keys(),"Element keys")
                        if (len(element) == 1) and (type(list(element.keys())[0]) == str) and \
                        ((type(list(element.values())[0]) == str) or (type(list(element.values())[0]) == float) \
                        or (type(list(element.values())[0]) == int)):
                            if key == 'array':
                                lst.append(json_recursion({list(element.keys())[0]:list(element.values())[0]},key_value ,lst))
                            else:
                                lst.append(json_recursion({list(element.keys())[0]:list(element.values())[0]},key ,lst))
                        else:
                            for key_in_list in element.keys():
#                                 print(key_in_list,"Key considered")
#                                 print(element[key_in_list],"Element \n\n\n")
    #                             key_ans, value_ans = json_recursion(element[key_in_list],key_in_list ,lst)
    #                             lst[key_ans] = value_ans
                                lst.append(json_recursion(element[key_in_list],key_in_list ,lst))

    return lst

if __name__ == '__main__':
    main()
