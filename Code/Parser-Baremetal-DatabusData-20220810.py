#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 10 10:04:07 2022

@author: kruthika.kulkarni
"""

import glob

import warnings
warnings.filterwarnings('ignore')
import pandas as pd
import time
import json

from datetime import datetime, timedelta
from multiprocess import Pool

def main():
    start = time.time()
    # List all json files to be converted to csv
    #json_files = glob.glob("")
    json_files = ['/Users/kruthika.kulkarni/Downloads/baremetal_2022-08-09_09:33:43.831310.txt']
    print(len(json_files))
    # Output_path to store csv
    output_path = "/Users/kruthika.kulkarni/Downloads/"
    # Give the list of features. If you want to convert the entire json to CSV irrespective of the performance metric, give an empty list
    feat_list = []
    p = Pool(4)
    arguments = [(file,output_path, feat_list) for file in json_files]
    # for file in json_files:
    #     common_data(file, output_path, feat_list)
    p.starmap(common_data, arguments)
    end = time.time()
    print("Time Taken: ", end - start)
    
def common_data(file, output_path, feat_list):
    condition_main = False
    condition = True
    if len(feat_list) == 0:
        condition_main = True
    lst_for_df = []
    for line in open(file, 'r'):
        if condition_main == False:
            condition = any(ext in line for ext in feat_list)
            
    
        if condition:
            try:
                item = eval(eval(line).decode('UTF-8'))['entries']
            except ValueError:
                continue

            if item is None:
                pass
            else:
                for val in item:
                    lst = []
                    final_lst = []
                    dict_to_consider_temp = {}
                    final = json_recursion(val,"ca.dataedu.Entrie",lst)
                    final = [x for x in final if type(x) is dict]
                    lst_of_listed_values = []
                    for ele in final:
                        if ele not in final_lst:
                            final_lst.append(ele)
                    

                    for ele in final_lst:
                        if type(list(ele.values())[0]) != list:
                            dict_to_consider_temp[list(ele.keys())[0]] = list(ele.values())[0]
                        else:
                            lst_of_listed_values.append(ele)
                            
                    if len(lst_of_listed_values) == 0:
                        lst_for_df.append(dict_to_consider_temp)
                    else: 
                        if len(list(set([len(list(ele.values())[0]) for ele in lst_of_listed_values]))) == 1:
                            for i in range(len(list(lst_of_listed_values[0].values())[0])):
                                dict_to_consider = dict_to_consider_temp.copy()
                                for lst_ele in lst_of_listed_values:
                                    dict_to_consider[list(lst_ele.keys())[0]] = list(lst_ele.values())[0][i]
                                lst_for_df.append(dict_to_consider)
                                    
    if len(lst_for_df) > 0:           
        final = pd.DataFrame(lst_for_df)
        
        print(final.shape)
        final.to_csv(output_path+file.split("/")[-1].replace(".txt",".csv"), index = False) 
        


def convert_time(s):
    s1 = str(s.values[0])
    time_new = datetime.strptime(s1[-5:], '%H:%M').time()
    return datetime.fromisoformat(s1[:-6])+timedelta(hours = time_new.hour,minutes=time_new.minute)


def json_recursion(dictionery,key_value,lst):
    """
    dictionery : Json Line which is being converted to a row in dataframe (dict)
    key_value : the main key of the above dictionery (string)
    lst : The list keeping track of nested values of the dictionery which make up the row of the dataframe.
          The user has to always give an empty list. It gets filled through recursion. 
          Hence we do not need to pass this argument in any function.
    
    """
    main_dct = {}
    if (len(dictionery) == 0) and type(dictionery) == dict:
        main_dct[key_value] = ''
        return main_dct
    elif (len(dictionery.keys()) == 1) and (type(dictionery[list(dictionery.keys())[0]]) != dict) and \
    (type(dictionery[list(dictionery.keys())[0]]) != list):
        main_dct[key_value] = dictionery[list(dictionery.keys())[0]]
        return main_dct
    
    elif (len(dictionery.keys()) == 1) and ((type(dictionery[list(dictionery.keys())[0]]) == list)\
    and len([type(x) for x in dictionery[list(dictionery.keys())[0]] if type(x) not in [dict, list]]) > 0):
        main_dct[key_value] = dictionery[list(dictionery.keys())[0]]
        return main_dct
        
    elif len([x for x in list(dictionery.values()) if type(x) == str]) == len(list(dictionery.values())):
        for key_in_dict, value_in_dict in dictionery.items():
            lst.append(json_recursion({key_in_dict:value_in_dict},key_in_dict ,lst))
    else:
        for key in dictionery.keys():
            if (type(dictionery[key]) != dict) and (type(dictionery[key]) != list):
                lst.append(json_recursion({key:dictionery[key]},key ,lst))
            if type(dictionery[key]) == dict:
                lst.append(json_recursion(dictionery[key], key,lst))
            elif type(dictionery[key]) == list:
                if (dict not in [type(x) for x in dictionery[key]])\
                and (list not in [type(x) for x in dictionery[key]]):
                    lst.append(json_recursion({key:(dictionery[key])}, key, lst))
                else:
                    for element in dictionery[key]:
                        if type(element) == dict:
                            if (len(element) == 1) and (type(list(element.keys())[0]) == str) and \
                            ((type(list(element.values())[0]) == str) or (type(list(element.values())[0]) == float) \
                            or (type(list(element.values())[0]) == int)):
                                if key == 'array':
                                    lst.append(json_recursion({list(element.keys())[0]:list(element.values())[0]},key_value ,lst))
                                else:
                                    lst.append(json_recursion({list(element.keys())[0]:list(element.values())[0]},key ,lst))
                            else:
                                for key_in_list in element.keys():
                                    if type(element[key_in_list]) == dict:
                                        lst.append(json_recursion(element[key_in_list],key_in_list ,lst))
                                    else:
                                        lst.append(json_recursion(element,key_in_list ,lst))

    return lst



if __name__ == '__main__':
    main()
