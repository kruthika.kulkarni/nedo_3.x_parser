#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  4 11:27:12 2022

@author: kruthika.kulkarni
"""

import xml.etree.ElementTree as ETree
import pandas as pd
import glob
import time
from multiprocess import Pool
from datetime import datetime, timedelta
import re
import numpy as np

# PM files
def main():
    """
    files : Give the list of XML files to be parsed
    output_path : Location where the parsed CSV should be saved.
    p : Change the number of Pool based on your VM capacity.
    
    """

    start = time.time()
    files = glob.glob("/Users/kruthika.kulkarni/Downloads/pmfiles/*.xml")
    output_path = "/Users/kruthika.kulkarni/Downloads/pmfiles/CSV/"
    p = Pool(4)
    
    arguments = [(file,output_path) for file in files]
    p.starmap(get_csv, arguments)
    end = time.time()
    print("Time Taken:", end - start)

def get_csv(file,output_path):
    prstree = ETree.parse(file)
    root = prstree.getroot()
    lst_check = xml_recursive(root, [])
    metakey = []
    metavalue = []
    lst_check = [x for x in lst_check if len(x)>0]
    lst_check = [{list(x.keys())[0].split("}")[-1]:list(x.values())[0]} for x in lst_check]
    for ele in lst_check:
        if 'value' in list(ele.keys())[0].lower():
            ele['Value'] = ele[list(ele.keys())[0]]
            del ele[list(ele.keys())[0]]
            
    lst_of_keys = list(set([list(x.keys())[0] for x in lst_check]))
    for key in lst_of_keys:
        if len([list(x.keys())[0] for x in lst_check if list(x.keys())[0] == key]) == 1:
            metakey.append(key)
            metavalue.append([list(x.values())[0] for x in lst_check if list(x.keys())[0] == key][0])
    df = pd.DataFrame()
    rows = []
    temp = []
    for l,ele in enumerate(lst_check):

        if list(ele.keys())[0] not in metakey:
            if (list(ele.keys())[0] == 'measInfoId') and len(temp) > 0:
                rows.append(temp)
                temp = []
            temp.append(ele)
            
        if l == len(lst_check) - 1:
            rows.append(temp)
    for ele in rows:
        completed_tags = []
        temp_df = pd.DataFrame()
        num_of_counters = len([x for x in ele if 'MeasType' in x.keys()])
        num_of_du = len([x for x in ele if 'measObjInstId' in x.keys()])
        num_of_rows =  num_of_counters * num_of_du

        counters = [list(x.values())[0] for x in ele if 'MeasType' in x.keys()]
        counters = counters * num_of_du
        value_of_du = [list(x.values())[0] for x in ele if 'measObjInstId' in x.keys()]
        temp_df = pd.DataFrame(np.array(counters))
        completed_tags.append('MeasType')
        temp_df.columns = ["counter"]
        for i in range(num_of_du):
            temp_df.loc[i*num_of_counters : (i+1)*num_of_counters-1, 'DU'] = value_of_du[i]
        completed_tags.append('measObjInstId')
        for item in ele:
            key_item = [list(x.keys())[0] for x in ele if list(item.keys())[0] in x.keys()]
            value_item = [list(x.values())[0] for x in ele if list(item.keys())[0] in x.keys()]
            if key_item[0] not in completed_tags:
                if len(key_item) == 1:
                    temp_df[list(item.keys())[0]] = list(item.values())[0]
                    completed_tags.append(key_item[0])
                elif len(key_item) == num_of_rows:
                    temp_df[key_item[0]] = np.array(value_item)
                    completed_tags.append(key_item[0])
        if df.shape[0] == 0:
            df = temp_df
        else:
            df = df.append(temp_df)

    for i,key in enumerate(metakey):
        df[key] = metavalue[i]
        
    df.to_csv(output_path+file.split('/')[-1].replace('xml','csv'), index = False)
            
def xml_recursive(root, lst):
    main_dict = {}
    if root.text == None:
        return {}
    elif (len(re.findall(r'[^\n" "]', root.text)) > 0) and (len(root.attrib) == 0) and \
    len(root) == 0:
        main_dict[root.tag] = root.text
        return main_dict
    elif (len(re.findall(r'[^\n" "]', root.text)) > 0) and (len(root.attrib) > 0) and \
    len(root) == 0:
        main_dict[root.tag] = root.text
        for key,val in root.attrib:
            main_dict[key] = val
        return main_dict
    elif (len(re.findall(r'[^\n" "]', root.text)) == 0) and (len(root.attrib) > 0) and \
    len(root) == 0:
        for key,val in root.attrib:
            main_dict[key] = val
        return main_dict
    elif len(root) > 0:
        if len(root.attrib) > 0:
            for key,val in root.attrib.items():
                lst.append({key:val})
        for i,child in enumerate(root):
            temp = xml_recursive(child, lst)
            if (type(temp) == dict) and len(temp) > 0:
                lst.append(temp)
            
            
    return lst    

if __name__ == '__main__':
    main()        
            
            
            
        
