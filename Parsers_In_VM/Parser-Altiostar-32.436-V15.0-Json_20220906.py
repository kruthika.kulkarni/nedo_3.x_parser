#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep  6 18:40:31 2022

@author: kruthika.kulkarni
"""

import xml.etree.ElementTree as ETree
import pandas as pd
import glob
import time
from multiprocess import Pool
from datetime import datetime, timedelta
import re
import numpy as np
import os
import json

# PM files
def main():
    """
    THIS CODE HAS NOT BEEN TESTED AS WE HAVEN'T RECIEVED OBF ENRICHED JSON OF NEDO 3.1 DATA. 
    THE VALUES TO BE PROVIDED BY THE USER
    input_path : Path where the altiostar xml in json format  (Data for Nedo 3-1) files exist.
    EXAMPLE : "/home/centos/data/cadvisor/" 
    output_path : Location where the parsed csv should be stored.
    EXAMPLE : "/home/centos/data/cadvisor/CSV/"
    feat_list : Give the list of features. If you want to convert the entire json to CSV irrespective of the performance metric, give an empty list
    EXAMPLE : ['DLThroughput.pcg1','ULThrougput.pcg1']
    p : Pool values based on the VM configuration.
    EXAMPLE : p = 16
    
    """
    
    start = time.time()
    #Location of json files gotten from databus
    input_path = "/Users/kruthika.kulkarni/Documents/Projects/Nedo-3.x/Nedo3.1/1_pm/"
    #Location to store output csv
    output_path = "/Users/kruthika.kulkarni/Documents/Projects/Nedo-3.x/Nedo3.1/1_pm/"
    feat_list = []
    p = Pool(4)
    if input_path[-1] != "/":
        input_path = input_path + "/"
    if output_path[-1] != "/":
        output_path = output_path + "/"    
    json_files = glob.glob(input_path+"*.json")


    files_completed_in_output_path = glob.glob(output_path+"*.csv")
    files_completed_in_output_path = [input_path+x.split("/")[-1].replace(".csv",".json") for x in files_completed_in_output_path]
    if len(files_completed_in_output_path) > 0:
        json_files = [x for x in json_files if x not in files_completed_in_output_path]   
   
    
    if len(json_files) > 0:
        arguments = [(file,output_path, feat_list) for file in json_files]
        p.starmap(get_csv, arguments)
    else:
        print("No files left")
    end = time.time()
    print("Time Taken:", end - start)

def get_csv(file,output_path, feat_list):
    valueJson = [json.loads(line) for line in open(file,'r')]
    val = valueJson[0]
    xmlstring =  val['value']['string']
    prstree = ETree.ElementTree(ETree.fromstring(xmlstring))
    root = prstree.getroot()
    lst_check = xml_recursive(root, [])
    metakey = []
    metavalue = []
    lst_check = [x for x in lst_check if len(x)>0]
    lst_check = [{list(x.keys())[0].split("}")[-1]:list(x.values())[0]} for x in lst_check]
    for ele in lst_check:
        if 'value' in list(ele.keys())[0].lower():
            ele['Value'] = ele[list(ele.keys())[0]]
            del ele[list(ele.keys())[0]]
            
    lst_of_keys = list(set([list(x.keys())[0] for x in lst_check]))
    for key in lst_of_keys:
        if len([list(x.keys())[0] for x in lst_check if list(x.keys())[0] == key]) == 1:
            metakey.append(key)
            metavalue.append([list(x.values())[0] for x in lst_check if list(x.keys())[0] == key][0])
    df = pd.DataFrame()
    rows = []
    temp = []
    for l,ele in enumerate(lst_check):

        if list(ele.keys())[0] not in metakey:
            if (list(ele.keys())[0] == 'measInfoId') and len(temp) > 0:
                rows.append(temp)
                temp = []
            temp.append(ele)
            
        if l == len(lst_check) - 1:
            rows.append(temp)
    for ele in rows:
        completed_tags = []
        temp_df = pd.DataFrame()
        num_of_counters = len([x for x in ele if 'MeasType' in x.keys()])
        num_of_du = len([x for x in ele if 'measObjInstId' in x.keys()])
        num_of_rows =  num_of_counters * num_of_du

        counters = [list(x.values())[0] for x in ele if 'MeasType' in x.keys()]
        counters = counters * num_of_du
        value_of_du = [list(x.values())[0] for x in ele if 'measObjInstId' in x.keys()]
        temp_df = pd.DataFrame(np.array(counters))
        completed_tags.append('MeasType')
        temp_df.columns = ["counter"]
        for i in range(num_of_du):
            temp_df.loc[i*num_of_counters : (i+1)*num_of_counters-1, 'DU'] = value_of_du[i]
        completed_tags.append('measObjInstId')
        for item in ele:
            key_item = [list(x.keys())[0] for x in ele if list(item.keys())[0] in x.keys()]
            value_item = [list(x.values())[0] for x in ele if list(item.keys())[0] in x.keys()]
            if key_item[0] not in completed_tags:
                if len(key_item) == 1:
                    temp_df[list(item.keys())[0]] = list(item.values())[0]
                    completed_tags.append(key_item[0])
                elif len(key_item) == num_of_rows:
                    temp_df[key_item[0]] = np.array(value_item)
                    completed_tags.append(key_item[0])
        if df.shape[0] == 0:
            df = temp_df
        else:
            df = df.append(temp_df)

    for i,key in enumerate(metakey):
        df[key] = metavalue[i]
    
    if len(feat_list) > 0:
        df = df[df['counter'].isin(feat_list)]    
    df.to_csv(output_path+file.split('/')[-1].replace('json','csv'), index = False)
            
def xml_recursive(root, lst):
    main_dict = {}
    if root.text == None:
        return {}
    elif (len(re.findall(r'[^\n" "]', root.text)) > 0) and (len(root.attrib) == 0) and \
    len(root) == 0:
        main_dict[root.tag] = root.text
        return main_dict
    elif (len(re.findall(r'[^\n" "]', root.text)) > 0) and (len(root.attrib) > 0) and \
    len(root) == 0:
        main_dict[root.tag] = root.text
        for key,val in root.attrib:
            main_dict[key] = val
        return main_dict
    elif (len(re.findall(r'[^\n" "]', root.text)) == 0) and (len(root.attrib) > 0) and \
    len(root) == 0:
        for key,val in root.attrib:
            main_dict[key] = val
        return main_dict
    elif len(root) > 0:
        if len(root.attrib) > 0:
            for key,val in root.attrib.items():
                lst.append({key:val})
        for i,child in enumerate(root):
            temp = xml_recursive(child, lst)
            if (type(temp) == dict) and len(temp) > 0:
                lst.append(temp)
            
            
    return lst    

if __name__ == '__main__':
    main()        
            
            
            
        
