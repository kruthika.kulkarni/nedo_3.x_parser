This is the list of .py files to use and points to keep in mind. 

Please note that, all codes are written to run once at the moment. For an infinite loop in demo, you will need to add a while True: main() statement. 

1. Parser for Altiostar Data from Phase-2 if given from MinIO bucket.(Avro to Json converted data) 
File Name : Parser-Altiostar-Phase2-Json-20220906.py
User Input : 
            input_path : Path where the altiostar json files exist
            EXAMPLE : "/home/centos/data/altiostar/"
            output_path : Location where the parsed csv should be stored.
            EXAMPLE : "/home/centos/data/altiostar/CSV/"
            feat_list : If you know the list of performance metrics that you need in a CSV format, specify them as a list here. If you want all performance metrics to be parsed, given an empty list.
            EXAMPLE : ['DLThroughputVol.pcg1','DLThroughputVol.pcg2']
            p : Pool values based on the VM configuration.
            EXAMPLE : 4

2. Parser for Altiostar Data from Phase-2 if given from Databus. (Directly an xml file)
File Name : Parser-Altiostar_NedoPhase2-Databus_32.436_v15_20220906.py
User Input :
            input_path : Path where the altiostar xml files from databus exist
            EXAMPLE : "/home/centos/data/altiostar/"
            output_path : Location where the parsed csv should be stored.
            EXAMPLE : "/home/centos/data/altiostar/CSV"
            feat_list : If you know the list of performance metrics that you need in a CSV format, specify them as a list here. If you want all performance metrics to be parsed, given an empty list.
            EXAMPLE : ['DLThroughputVol.pcg1','DLThroughputVol.pcg2']
            p : Pool values based on the VM configuration.
            EXAMPLE : 4

3. Parser for Altiostar Data for Nedo 3-1 based on the example folder (1_pm) which contained xmls. 
File Name : Parser-Altiostar-32.436-V15.0-xml_20220906.py
User Input :
            input_path : Path where the altiostar xml files exist. The XML should be of the form as shared by Prasanna (1_pm file)
            EXAMPLE : "/home/centos/data/altiostar/"
            output_path : Location where the parsed csv should be stored.
            EXAMPLE : "/home/centos/data/altiostar/CSV/"
            feat_list : If you know the list of performance metrics that you need in a CSV format, specify them as a list here. If you want all performance metrics to be parsed, given an empty list.
            EXAMPLE : ['DLThroughputVol.pcg1','DLThroughputVol.pcg2']
            p : Pool values based on the VM configuration.
            EXAMPLE : 4

4. Parser for Altiostar Data for Nedo 3-1 from MinIo bucket as JSON.
Note that, no example jsons were available. So this parser might need changes before use as it has never been tested. 
File Name : Parser-Altiostar-32.436-V15.0-Json_20220906.py
User Input :
            input_path : Path where the altiostar json files exist
            EXAMPLE : "/home/centos/data/altiostar/"
            output_path : Location where the parsed csv should be stored.
            EXAMPLE : "/home/centos/data/altiostar/CSV/"
            feat_list : If you know the list of performance metrics that you need in a CSV format, specify them as a list here. If you want all performance metrics to be parsed, given an empty list.
            EXAMPLE : ['DLThroughputVol.pcg1','DLThroughputVol.pcg2']
            p : Pool values based on the VM configuration.
            EXAMPLE : 4

5. Parser for CADVISOR, NECPM, KSMPM data coming from databus. (text files to CSV)
File Name: Parser-JSON-DatabusData-20220810.py
User Input :
            input_path : Path where the cadvisor, necpm, ksmpm text from databus files exist
            EXAMPLE : "/home/centos/data/cadvisor/"
            output_path : Location where the parsed csv should be stored.
            EXAMPLE : "/home/centos/data/cadvisor/CSV/"
            feat_list : If you know the list of performance metrics that you need in a CSV format, specify them as a list here. If you want all performance metrics to be parsed, given an empty list. 
            EXAMPLE : ['DLThroughputVol.pcg1','DLThroughputVol.pcg2']
            p : Pool values based on the VM configuration.
            EXAMPLE : 4


6. Parser for CADVISOR, NECPM, KSMPM data coming from MinIO bucket. (avro to json to CSV)
File Name: Parser-Json-Common.py
User Input :
            input_path : Path where the cadvisor, necpm, ksmpm json files exist
            EXAMPLE : "/home/centos/data/cadvisor/"
            output_path : Location where the parsed csv should be stored.
            EXAMPLE : "/home/centos/data/cadvisor/CSV/"
            feat_list : If you know the list of performance metrics that you need in a CSV format, specify them as a list here. If you want all performance metrics to be parsed, given an empty list.
            EXAMPLE : ['DLThroughputVol.pcg1','DLThroughputVol.pcg2']
            p : Pool values based on the VM configuration.
            EXAMPLE : 4

7. Parser for Baremetal for data coming from databus (text file to CSV)
File Name: Parser-Baremetal-DatabusData-20220810.py
User Input :
            input_path : Path where the baremetal text files from databus exist
            EXAMPLE : "/home/centos/data/baremetal/"
            output_path : Location where the parsed csv should be stored.
            EXAMPLE : "/home/centos/data/baremetal/CSV/"
            feat_list : If you know the list of performance metrics that you need in a CSV format, specify them as a list here. If you want all performance metrics to be parsed, given an empty list.
            EXAMPLE : ['DLThroughputVol.pcg1','DLThroughputVol.pcg2']
            p : Pool values based on the VM configuration.
            EXAMPLE : 4

8. Parser for Baremetal for data coming from MinIO bucket (avro to json to csv)
File Name : Parser-Json-Baremetal.py
User Input :
            input_path : Path where the baremetal json files exist
            EXAMPLE : "/home/centos/data/baremetal/"
            output_path : Location where the parsed csv should be stored.
            EXAMPLE : "/home/centos/data/baremetal/CSV/"
            feat_list : If you know the list of performance metrics that you need in a CSV format, specify them as a list here. If you want all performance metrics to be parsed, given an empty list.
            EXAMPLE : ['DLThroughputVol.pcg1','DLThroughputVol.pcg2']
            p : Pool values based on the VM configuration.
            EXAMPLE : 4
