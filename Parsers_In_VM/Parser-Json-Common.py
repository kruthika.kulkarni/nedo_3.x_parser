#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 18 16:02:24 2022

@author: kruthika.kulkarni
"""
import numpy as np
import glob
import os
import warnings
warnings.filterwarnings('ignore')
import pandas as pd
import time
import json

from datetime import datetime, timedelta
from multiprocess import Pool

def main():
    """
    THE VALUES TO BE PROVIDED BY THE USER
    input_path : Path where the cadvisor, necpm, ksmpm data exists as json (Data from MinIO)
    EXAMPLE : "/home/centos/data/cadvisor/"
    output_path : Location where the parsed csv should be stored.
    EXAMPLE : "/home/centos/data/cadvisor/CSV/"
    feat_list : Give the list of features. If you want to convert the entire json to CSV irrespective of the performance metric, give an empty list
    EXAMPLE : ['DLThroughput.pcg1','ULThrougput.pcg1']
    p : Pool values based on the VM configuration.
    EXAMPLE : p = 16

    """

    start = time.time()
    #Location of json files
    input_path = "/home/centos/Parser/Phase3_OBFData/necpm2/"
    #Location to store output
    output_path = "/home/centos/Parser/Phase3_OBFData/necpm2/"
    # Give the list of features. If you want to convert the entire json to CSV irrespective of the performance metric, give an empty list
    feat_list = []
    p = Pool(4)

    if input_path[-1] != "/":
        input_path = input_path + "/"
    if output_path[-1] != "/":
        output_path = output_path + "/"

    json_files = glob.glob(input_path+"*.json")


    files_completed_in_output_path = glob.glob(output_path+"*.csv")
    files_completed_in_output_path = [input_path+x.split("/")[-1].replace(".csv",".json") for x in files_completed_in_output_path]
    if len(files_completed_in_output_path) > 0:
        json_files = [x for x in json_files if x not in files_completed_in_output_path]

    if len(json_files) > 0:
        arguments = [(file,output_path, feat_list) for file in json_files]
        p.starmap(common_data, arguments)
    else:
        print("No files to parse")
    end = time.time()
    print("Time Taken: ", end - start)
    
def common_data(file, output_path, feat_list):
    condition_main = False
    condition = True
    if len(feat_list) == 0:
        condition_main = True
    lst_for_df = []
    for line in open(file, 'r'):
        if condition_main == False:
            condition = any(ext in str(line) for ext in feat_list)
            
    
        if condition:
            try:
                item = json.loads(line)
            except ValueError:
                continue
            if (type(item['value']['entries']) == dict) and ('array' in item['value']['entries'].keys()):
                item_subset = item['value']['entries']['array']
            else:
                item_subset = item['value']['entries']
            if item_subset is None:
                pass
            else:
                for val in item_subset:
                    lst = []
                    dict_to_consider = {}
                    final = json_recursion(val,"ca.dataedu.Entrie",lst)
                    final = [x for x in final if type(x) is dict]
                    for ele in final:
                        dict_to_consider[list(ele.keys())[0]] = list(ele.values())[0]
                    lst_for_df.append(dict_to_consider)
        
    if len(lst_for_df) > 0:           
        final = pd.DataFrame(lst_for_df)
        # final['timeStamp1']=final[['timeStamp']].apply(convert_time, axis = 1)
        # final.drop(columns = ['timeStamp'], inplace = True)
    
        final.to_csv(output_path+file.split("/")[-1].replace(".json",".csv"), index = False) 
        


def convert_time(s):
    s1 = str(s.values[0])
    time_new = datetime.strptime(s1[-5:], '%H:%M').time()
    return datetime.fromisoformat(s1[:-6])+timedelta(hours = time_new.hour,minutes=time_new.minute)


def json_recursion(dictionery,key_value,lst):
    """
    dictionery : Json Line which is being converted to a row in dataframe (dict)
    key_value : the main key of the above dictionery (string)
    lst : The list keeping track of nested values of the dictionery which make up the row of the dataframe.
          The user has to always give an empty list. It gets filled through recursion. 
          Hence we do not need to pass this argument in any function.
    
    """
#     print(dictionery,"\n",key_value,"Key",lst,"Initial \n\n")
    main_dct = {}
    if (len(dictionery.keys()) == 1) and (type(dictionery[list(dictionery.keys())[0]]) != dict) and \
    (type(dictionery[list(dictionery.keys())[0]]) != list):
#         return key_value, dictionery[list(dictionery.keys())[0]]
        main_dct[key_value] = dictionery[list(dictionery.keys())[0]]
#         print(main_dct,"Done")
        return main_dct
        
    elif len([x for x in list(dictionery.values()) if type(x) == str]) == len(list(dictionery.values())):
#         print("Comes here")
        for key_in_dict, value_in_dict in dictionery.items():
#             print(key_in_dict, value_in_dict, "key_value_pair")
            lst.append(json_recursion({key_in_dict:value_in_dict},key_in_dict ,lst))
    else:
        for key in dictionery.keys():
#             print(key,dictionery[key],"Key inside")
            if (type(dictionery[key]) != dict) and (type(dictionery[key]) != list):
                lst.append(json_recursion({key:dictionery[key]},key ,lst))
            if type(dictionery[key]) == dict:
#                 key_ans, value_ans = json_recursion(dictionery[key], key,lst)
#                 lst[key_ans] = value_ans
                lst.append(json_recursion(dictionery[key], key,lst))
            elif type(dictionery[key]) == list:
#                 print("Inside list", key)
                for element in dictionery[key]:
                    if type(element) == dict:
#                         print(element.keys(),"Element keys")
                        if (len(element) == 1) and (type(list(element.keys())[0]) == str) and \
                        ((type(list(element.values())[0]) == str) or (type(list(element.values())[0]) == float) \
                        or (type(list(element.values())[0]) == int)):
                            if key == 'array':
                                lst.append(json_recursion({list(element.keys())[0]:list(element.values())[0]},key_value ,lst))
                            else:
                                lst.append(json_recursion({list(element.keys())[0]:list(element.values())[0]},key ,lst))
                        else:
                            for key_in_list in element.keys():
#                                 print(key_in_list,"Key considered")
#                                 print(element[key_in_list],"Element \n\n\n")
    #                             key_ans, value_ans = json_recursion(element[key_in_list],key_in_list ,lst)
    #                             lst[key_ans] = value_ans
                                lst.append(json_recursion(element[key_in_list],key_in_list ,lst))

    return lst

if __name__ == '__main__':
    main()
