import pandas as pd
import numpy as np
import glob
def main():
    input_path = "/home/centos/Parser/Phase3_OBFData/necpm1/"
    csv_files = glob.glob(input_path+"*.csv")
    lst_of_feat = []
    for file in csv_files:
        df = pd.read_csv(file)
        lst_of_feat = list(set(lst_of_feat + list(df['performanceMetric'].unique())))
    pd.DataFrame(np.array(lst_of_feat)).to_csv(input_path+input_path.split("/")[-2]+".csv")

if __name__ == '__main__':
    main() 
