#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug  9 16:17:20 2022

@author: kruthika.kulkarni
"""

import xml.etree.ElementTree as ETree
import pandas as pd
import glob
import time
from multiprocess import Pool
from datetime import datetime, timedelta
import re
import numpy as np
import json
import os




# PM files
def main():
    """
    THE VALUES TO BE PROVIDED BY THE USER
    input_path : Path where the altiostar xml in json format (from Phase 2) files exist.
    EXAMPLE : "/home/centos/data/cadvisor/" 
    output_path : Location where the parsed csv should be stored.
    EXAMPLE : "/home/centos/data/cadvisor/CSV/"
    feat_list : Give the list of features. If you want to convert the entire json to CSV irrespective of the performance metric, give an empty list
    EXAMPLE : ['DLThroughput.pcg1','ULThrougput.pcg1']
    p : Pool values based on the VM configuration.
    EXAMPLE : p = 16
    
    """
    
    start = time.time()
    #Location of json files gotten from databus
    input_path = "/home/centos/Parser/Phase3_OBFData/altiostar"
    #Location to store output csv
    output_path = "/home/centos/Parser/Phase3_OBFData/altiostar/CSV"
    feat_list = []
    p = Pool(4)
    if input_path[-1] != "/":
        input_path = input_path + "/"
    if output_path[-1] != "/":
        output_path = output_path + "/"
    
    json_files = glob.glob(input_path+"*.json")


    files_completed_in_output_path = glob.glob(output_path+"*.csv")
    files_completed_in_output_path = [input_path+x.split("/")[-1].replace(".csv",".json") for x in files_completed_in_output_path]
    if len(files_completed_in_output_path) > 0:
        json_files = [x for x in json_files if x not in files_completed_in_output_path]  

   
    if len(json_files) > 0:
        arguments = [(file,output_path, feat_list) for file in json_files]
        p.starmap(get_csv, arguments)
    else:
        print("No files left")
    end = time.time()
    print("Time Taken:", end - start)

def get_csv(file,output_path, feat_list):
    valueJson = [json.loads(line) for line in open(file,'r')]
    val = valueJson[0]
    xmlstring =  val['value']['string']
    prstree = ETree.ElementTree(ETree.fromstring(xmlstring))
    root = prstree.getroot()
    lst_check = xml_recursive(root, [])
    metakey = []
    metavalue = []
    lst_check = [x for x in lst_check if len(x)>0]
    lst_check = [{list(x.keys())[0].split("}")[-1]:list(x.values())[0]} for x in lst_check]
    for ele in lst_check:
        if 'value' in list(ele.keys())[0].lower():
            ele['Value'] = ele[list(ele.keys())[0]]
            del ele[list(ele.keys())[0]]
            
    lst_of_keys = list(set([list(x.keys())[0] for x in lst_check]))
    for key in lst_of_keys:
        if len([list(x.keys())[0] for x in lst_check if list(x.keys())[0] == key]) == 1:
            metakey.append(key)
            metavalue.append([list(x.values())[0] for x in lst_check if list(x.keys())[0] == key][0])
    df = pd.DataFrame()
    rows = []
    temp = []
    for l,ele in enumerate(lst_check):

        if list(ele.keys())[0] not in metakey:
            if (list(ele.keys())[0] == 'measInfoId') and len(temp) > 0:
                rows.append(temp)
                temp = []
            temp.append(ele)
            
        if l == len(lst_check) - 1:
            rows.append(temp)

    for ele in rows:
        completed_tags = []
        temp_df = pd.DataFrame()

#         num_of_rows =  num_of_counters * num_of_du

        counters = [list(x.values())[0] for x in ele if 'measTypes' in x.keys()][0].split(" ")
        value_of_du = [list(x.values())[0] for x in ele if 'measObjLdn' in x.keys()]
        num_of_counters = len(counters)
        num_of_du = len(value_of_du)
        num_of_rows =  num_of_counters * num_of_du
        counters = counters * num_of_du
        temp_df = pd.DataFrame(np.array(counters))
        completed_tags.append('measTypes')
        temp_df.columns = ["counter"]

        for i in range(num_of_du):
            temp_df.loc[i*num_of_counters : (i+1)*num_of_counters-1, 'DU'] = value_of_du[i]
        completed_tags.append('measObjInstId')
        
        for item in ele:
            key_item = [list(x.keys())[0] for x in ele if list(item.keys())[0] in x.keys()]
            value_item = [list(x.values())[0] for x in ele if list(item.keys())[0] in x.keys()]
            if key_item[0] not in completed_tags:
                if key_item[0] != 'measResults':
                    temp_df[list(item.keys())[0]] = list(item.values())[0]
                    completed_tags.append(key_item[0])
                else:
                    final_list_of_values = []
                    value_item = [x.split(" ") for x in value_item]
                    for val in value_item:
                        final_list_of_values = final_list_of_values + val
                    if len(final_list_of_values) == num_of_rows:
                        temp_df[key_item[0]] = np.array(final_list_of_values)
                        completed_tags.append(key_item[0])
                    else:
                            print("Doesn't work, values not same as types")
        if df.shape[0] == 0:
            df = temp_df
        else:
            df = df.append(temp_df)

    for i,key in enumerate(metakey):
        df[key] = metavalue[i]
        
    #print(output_path+file.split('/')[-1].replace('json','csv'))
    if len(feat_list) > 0:
        df = df[df['counter'].isin(feat_list)]    
    df.to_csv(output_path+file.split('/')[-1].replace('json','csv'), index = False)
    

            
def xml_recursive(root, lst):
    main_dict = {}
    if (type(root) == dict) and len(root) == 1:
        main_dict = root
        return main_dict
    if (root.text == None) and (len(root.attrib) == 0):
        return {}
    elif (root.text == None) and (len(root.attrib) > 0):
        for key,val in root.attrib.items():
            return xml_recursive({root.tag+"_"+key:val}, lst)
    elif (len(re.findall(r'[^\n" "]', root.text)) > 0) and (len(root.attrib) == 0) and \
    len(root) == 0:
        main_dict[root.tag] = root.text
        return main_dict
    elif (len(re.findall(r'[^\n" "]', root.text)) > 0) and (len(root.attrib) > 0) and \
    len(root) == 0:
        main_dict[root.tag] = root.text
        for key,val in root.attrib:
            main_dict[key] = val
        return main_dict
    elif (len(re.findall(r'[^\n" "]', root.text)) == 0) and (len(root.attrib) > 0) and \
    len(root) == 0:
        for key,val in root.attrib:
            main_dict[key] = val
        return main_dict
    elif len(root) > 0:
        if len(root.attrib) > 0:
            for key,val in root.attrib.items():
                lst.append({key:val})
        for i,child in enumerate(root):
            temp = xml_recursive(child, lst)
            if (type(temp) == dict) and len(temp) > 0:
                lst.append(temp) 
            
    return lst    

if __name__ == '__main__':
    main()        
            
            
            
        
